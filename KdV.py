import numpy as np
import matplotlib.pyplot as plt
import ffmpeg

from scipy.integrate import odeint
from scipy.fftpack import diff as psdiff
from matplotlib.animation import FuncAnimation
from matplotlib import animation


def kdv_exact(x, c):
    ''' Exact Single Soliton Soltuion for the KdV equation '''
    u = 0.5*c*np.cosh(0.5*np.sqrt(c)*x)**(-2)
    return u

def kdv(u, t, L):
    ''' Finding the first derivative of time using the KdV equation'''
    ux = psdiff(u, period=L) # using psdiff which uses the fourier transform to calcutate the derivative
    uxxx = psdiff(u, period=L, order=3)

    ''' KdV equation '''   
    dudt = -6*u*ux - uxxx

    return dudt

def kdv_solution(u0, t, L):
    '''Solving the KdV equation using odeint '''
    sol = odeint(kdv, u0, t, args=(L,), mxstep=50000)
    return sol

def animate(L, R, sol):
    ''' Animating the plot over time '''
    
    fig = plt.figure('Animation')
    ax = plt.axes(xlabel = 'Position', ylabel = 'Amplitude', xlim=(0, L), ylim=(R[0], R[1]))
    line, = ax.plot([], [], lw=3)

    def init():
        line.set_data([], [])
        return line,

    def animate(i):

        y = sol[i]
        line.set_data(x, y)
        return line,

    plt.title('Korteweg-de Vries Eqn')

    anim = FuncAnimation(fig, animate, init_func=init,
                               frames=Nt, interval=30, blit=True)
    
    return anim

# Initalising the x values
L = 100.0
Nx = 120
x = np.linspace(0, L, Nx)
xp = x-0.5*L

# Initalsing the time values
T = 100.0
Nt = 500
t = np.linspace(0, T, Nt)

# Setting the intial conditions
u0 = kdv_exact(xp, 0.75)

print("Computing the solution.")
sol = kdv_solution(u0, t, L)

print("Plotting.")

plt.figure('Colourplot')
plt.imshow(sol)
plt.colorbar()
plt.xlabel('x')
plt.ylabel('t')
plt.title('Korteweg-de Vries on a Periodic Domain')
plt.show()

# Finding the maxiumum and minimum values 
R = [-np.max(-sol), np.max(sol)]
A = animate(L, R, sol)

#Option to save the figure
'''Writer = animation.writers["ffmpeg"]
writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)
A.save('KdV.mp4', writer=writer)'''